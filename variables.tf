variable "aws_region" {
  type        = string
  description = "AWS region to deploy to"
  default     = "us-east-1"
}

variable "component_name" {
  description = "Name of the component."
  type        = string
  default     = "lambda-read-and-load"
}

variable "aws_account_id" {
  description = "Environment this template would be deployed to"
  type        = map(string)
  default     = {}
}

### TAGS
variable "line_of_business" {
  description = "HIDS LOB that owns the resource."
  type        = string
  default     = "TECH"
}

variable "ado" {
  description = "HIDS ADO that owns the resource. The ServiceNow Contracts table is the system of record for the actual ADO names and LOB names."
  type        = string
  default     = "Kojitechs"
}

variable "tier" {
  description = "Network tier or layer where the resource resides. These tiers are represented in every VPC regardless of single-tenant or multi-tenant. For most resources in the Infrastructure and Security VPC, the TIER will be Management. But in some cases,such as Atlassian, the other tiers are relevant."
  type        = string
  default     = "APP"
}

variable "tech_poc_primary" {
  description = "Email Address of the Primary Technical Contact for the AWS resource."
  type        = string
  default     = "kojitechs@gmail.com"
}

variable "application" {
  description = "Logical name for the application. Mainly used for kojitechs. For an ADO/LOB owned application default to the LOB name."
  type        = string
  default     = "ECS-CLUSTER"
}

variable "builder" {
  description = "The name of the person who created the resource."
  type        = string
  default     = "kojitechs@gmail.com"
}

variable "application_owner" {
  description = "Email Address of the group who owns the application. This should be a distribution list and no an individual email if at all possible. Primarily used for Ventech-owned applications to indicate what group/department is responsible for the application using this resource. For an ADO/LOB owned application default to the LOB name."
  default     = "kojitechs@gmail.com"
}

variable "vpc" {
  description = "The VPC the resource resides in. We need this to differentiate from Lifecycle Environment due to INFRA and SEC. One of \"APP\", \"INFRA\", \"SEC\", \"ROUTING\"."
  type        = string
  default     = "APP"
}

variable "cell_name" {
  description = "The name of the cell."
  type        = string
  default     = "KOJITECHS"
}

variable "timeout" {
  description = "The amount of time your Lambda Function has to run in seconds."
  type        = number
  default     = 40
}

variable "slackwebhook_url" {
  type        = string
  description = "The URL to the slack webhook"
  default     = "SLACK_WEBHOOK_ENDPOINT"
}

variable "bucket" {
  type = list(any)
}

variable "json_table" {
  type        = string
  description = "The URL to the slack webhook"
  default     = "jsontable"
}

variable "csv_table" {
  type        = string
  description = "The URL to the slack webhook"
  default     = "csvtable"
}
