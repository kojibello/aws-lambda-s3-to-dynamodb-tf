

data "aws_iam_policy_document" "service" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_partition" "current" {}
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

data "template_file" "S3policyDocumentForLambda" {

  template = file("./templates/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.this[0].arn
  }
}


data "aws_iam_policy" "this" {
  name = "AmazonDynamoDBFullAccess"
}

data "aws_iam_policy_document" "cloudwatch_policy" {

  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}

resource "aws_iam_role" "default" {
  name_prefix        = "${var.component_name}-"
  assume_role_policy = data.aws_iam_policy_document.service.json
}

resource "aws_iam_policy" "S3policyDocumentForLambda" {

  name        = "S3policyDocumentForLambda"
  path        = "/"
  description = "Allow access to the ${var.component_name} app  to access S3 bucket"
  policy      = data.template_file.S3policyDocumentForLambda.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.default.name
  policy_arn = aws_iam_policy.S3policyDocumentForLambda.arn
}

resource "aws_iam_role_policy_attachment" "allow_access_to_dynamo_from_lambda_function" {
  role       = aws_iam_role.default.name
  policy_arn = data.aws_iam_policy.this.arn
}

resource "aws_iam_role_policy" "lambda_aws_cloudwatch_log_group" {

  name   = "lambdaawscloudwatchloggroup"
  role   = aws_iam_role.default.name
  policy = data.aws_iam_policy_document.cloudwatch_policy.json
}
