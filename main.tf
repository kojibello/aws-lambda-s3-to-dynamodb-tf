terraform {
  required_version = ">=v1.2.1"

  backend "s3" {
    bucket         = "kojitechs.aws.eks.with.terraform.tf"
    dynamodb_table = "terraform-lock"
    key            = "path/env/aws-lambda-s3-to-dynamodb"
    region         = "us-east-1"
    encrypt        = "true"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.18.0"
    }
  }

}


data "terraform_remote_state" "operational_environment" {
  backend = "s3"

  config = {
    region = "us-east-1"
    bucket = "operational.vpc.tf.kojitechs"
    key    = format("env:/%s/path/env", terraform.workspace)
  }
}


resource "aws_s3_bucket" "this" {
  count  = length(var.bucket)
  bucket = var.bucket[count.index]
}

locals {
  operational_state  = data.terraform_remote_state.operational_environment.outputs
  slack_token        = jsondecode(local.operational_state.secrets_version.slacktoken)["slacktoken"]
  lambda_description = "React on a put request to bucket with name ${aws_s3_bucket.this[0].id} and loading data to database"
  filename           = "./functions"
}

provider "aws" {
  region = var.aws_region
  assume_role {
    role_arn = "arn:aws:iam::${lookup(var.aws_account_id, terraform.workspace)}:role/Role_For-S3_Creation"
  }
  default_tags {
    tags = module.required_tags.aws_default_tags
  }
}

resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
  bucket = aws_s3_bucket.this[0].id
  lambda_function {
    lambda_function_arn = module.lambda_function.lambda_function_arn
    events              = ["s3:ObjectCreated:Put"]
  }
}
resource "aws_lambda_permission" "bucket_notification" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_function.lambda_function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.this[0].arn
}

module "required_tags" {
  source = "git::https://github.com/Bkoji1150/kojitechs-tf-aws-required-tags.git?ref=v1.0.0"

  line_of_business        = var.line_of_business
  ado                     = var.ado
  tier                    = var.tier
  operational_environment = upper(terraform.workspace)
  tech_poc_primary        = var.tech_poc_primary
  tech_poc_secondary      = var.builder
  application             = var.application
  builder                 = var.builder
  application_owner       = var.application_owner
  vpc                     = var.vpc
  cell_name               = var.cell_name
  component_name          = var.component_name
}

module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  depends_on    = [aws_s3_bucket.this]
  function_name = "${var.component_name}-function"
  description   = local.lambda_description
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.9"
  publish       = true
  timeout       = var.timeout

  lambda_role = aws_iam_role.default.arn
  create_role = false
  source_path = local.filename
  store_on_s3 = true
  s3_bucket   = aws_s3_bucket.this[1].id

  environment_variables = {
    SLACK_WEBHOOK_ENDPOINT = local.slack_token
    LOAD_TO_JSON           = var.json_table,
    LOAD_TO_CSV            = var.csv_table

  }
  tags = {
    Module = "${var.component_name}-function"
  }
}
