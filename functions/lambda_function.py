
import boto3
from botocore.exceptions import ClientError
import json
import os 
import urllib3
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
from dynamodb import createDynamodbTable
s3 = boto3.client('s3')

dynamodb = boto3.resource('dynamodb', 'us-east-1')
dynamodb_client = boto3.client('dynamodb', 'us-east-1')

def lambda_handler(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name']
    bucket_object = event['Records'][0]['s3']['object']['key']

    if bucket_object.endswith('.csv'):
        logger.info("Wring database to csv")
        get_csv_from_s3(bucket_object,bucket)
    elif bucket_object.endswith('.json'):
        logger.info("Wring database to json")
        get_json_from_s3(bucket_object,bucket)
    else:
        logger.info("The file uploaded is neither json of csv, please provide either json or csv")

  
def get_json_from_s3(key:str, bucket):
    """
    Get the contents of the file as JSON whose key is specified. to load to dynamodb
    """
    try:
        if os.environ['LOAD_TO_JSON'] not in dynamodb_client.list_tables()['TableNames']:
            createDynamodbTable(os.environ['LOAD_TO_JSON'])
    except ClientError as err:
        if err.response['Error']['Code'] == 'ResourceNotFoundException':
            logger.info(f"Table {os.environ['LOAD_TO_JSON']} does not exist")
        else:
            logger.error(err)   

    table = dynamodb.Table(os.environ['LOAD_TO_JSON'])
    response = s3.get_object(Bucket=bucket,Key=key)['Body'].read().decode('utf-8')   
    content_dict = json.loads(response)
    logger.info(dir(table))
    try:
        table.wait_until_exists()
        with table.batch_writer() as batch:
            for item in content_dict:
                batch.put_item(
                    Item=item
                    )
        slack_message(table)          
    except ClientError as err:
        logger.error(err)

def get_csv_from_s3(key:str, bucket):
    try:
        if os.environ['LOAD_TO_CSV'] not in dynamodb_client.list_tables()['TableNames']:
            createDynamodbTable(os.environ['LOAD_TO_CSV'], attr="S")
    except ClientError as err:
        if err.response['Error']['Code'] == 'ResourceNotFoundException':
            logger.info(f"Table {os.environ['LOAD_TO_JSON']} does not exist")
        else:
            logger.error(err)    
    try:
        table = dynamodb.Table(os.environ['LOAD_TO_CSV'])
        table.wait_until_exists()
        response = s3.get_object(Bucket=bucket, Key=key)
        data = response['Body'].read().decode('utf-8')
        supermarket = data.split("\n")
        for user in supermarket:
            logger.info(user)
            user_data = user.split(",")
            table.put_item(
                Item = {
                    "ID":       user_data[0],
                    "Address":  user_data[1],
                    "City":     user_data[2],
                    "State":    user_data[3],
                    "Country":  user_data[4],
                    "Name":     user_data[5],
                    "Employees": user_data[6]
                }
            )
        slack_message(table)    
    except ClientError as err:
        logger.error(err)
     
def slack_message(metadata):
    """Slack message to post to Slack"""
    webhook_url = os.environ['SLACK_WEBHOOK_ENDPOINT']
    message_body = "Successfully Loaded db data to Dynamodb on {}".format(metadata)
    http = urllib3.PoolManager()
    data = {"text": message_body}
    http.request("POST", webhook_url,body=json.dumps(data),headers = {"Content-Type": "application/json"})   
    
    return None               
             
