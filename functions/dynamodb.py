import boto3 
from botocore.exceptions import ClientError
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
client = boto3.client('dynamodb', 'us-east-1')

def createDynamodbTable(TableName, attr="N"):
    try:
        response = client.create_table(
            TableName=TableName,
            KeySchema=[
                {
                    'AttributeName': 'ID',
                    'KeyType': 'HASH'
                },
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'ID', 
                    'AttributeType': attr
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            },
        
            Tags=[
                {
                    'Key': 'Name',
                    'Value': TableName
                },
            ],
        )
    except Exception as err:
        if err.response['Error']['Code']=='ResourceInUseException':
            logger.error("Table already exits")
        else:
            logger.error(err.response['Error'])
    except ClientError as err:
        logger.error(err)
    else:
         logger.info("Table Created Successfully {}".format(response))
